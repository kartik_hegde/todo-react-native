import React, { useState } from "react";
import Context from "./Context";
import AsyncStorage from "@react-native-async-storage/async-storage";

const TodoState = (props) => {
  const header = {
    "Content-Type": "application/json",
  };
  const server = "http://192.168.0.104:5000";
  const [todos, settodos] = useState([{
    todo:"new",
    completed:true,
    email:"kat@gmail.com",
    id:1707487387488
  }]);
  
  
  //populate
  const populate = async () => {
    const user =
     await AsyncStorage.getItem("email")
    const response = await fetch(server + "/ops/fetchall", {
      method: "POST",
      headers: header,
      body: JSON.stringify({ email: user }),
    });
    const json = await response.json();
    settodos(json);
    return;
  };

  //add todo
  const addTodo = async (payload) => {
    const response = await fetch(server + "/ops/add", {
      method: "POST",
      headers: header,
      body: JSON.stringify(payload),
    });
    const status = response.status;
    if (status === 200) {
      settodos([...todos, payload]);
    }
    console.log(todos)
    return;
  };
//delete all function
  const deleteAll = async () => {
    const response = await fetch(server + "/ops/deleteAll", {
      method: "DELETE",
      headers: header,
    });
    const status = response.status;
    if (status === 200) {
      settodos([]);
    }

    return;
  };
//delete a specific todo
  const removeTodo = async (payload) => {
    const response = await fetch(server + "/ops/deleteTodo", {
      method: "DELETE",
      headers: header,
      body: JSON.stringify(payload),
    });
    const status = response.status;
    if (status === 200) {
      const filteredTodos = todos.filter((todo) => todo.id !== payload.id);
      settodos(filteredTodos);
    }

    return;
  };
// update description or mark done for a todo
  const handleUpdate = async (check, payload) => {
    
    const response = await fetch(server + "/ops/updateTodo", {
      method: "POST",
      headers: header,
      body: JSON.stringify({
        check: check,
        id: payload.id,
        todo: payload.todo,
      }),
    });
  

    return;
  };

  return (
    <Context.Provider
      value={{
        todos,
        settodos,
        populate,
        handleUpdate,
        deleteAll,
        addTodo,
        removeTodo,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export default TodoState;
