import { Stack } from "expo-router";
import TodoState from "../TodoState";

const RootLayout = () => {
  return (
    <TodoState>
      <Stack>
    
        <Stack.Screen
          name="(tabs)"
          options={{
            headerShown: false,
          }}
        ></Stack.Screen>
      </Stack>
    </TodoState>
  );
};
export default RootLayout;
