import { Tabs } from "expo-router";

const TabLayout = () => {
  return (
    <Tabs>
      <Tabs.Screen
        name="Index"
        options={{
          headerTitle: "Todo App",
        }}
      />
      <Tabs.Screen name="Login" ></Tabs.Screen>
      <Tabs.Screen name="Register"></Tabs.Screen>
    </Tabs>
  );
};
export default TabLayout;
