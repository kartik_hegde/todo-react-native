import React, { useState } from "react";
import { Button, StyleSheet, TextInput, View, Text } from "react-native";
import { useNavigation } from "expo-router";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Login = () => {
  const navigation = useNavigation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const server = "http://192.168.0.104:5000";

  const onFinish = async () => {
    try {
      const response = await fetch(server + "/user/createuser", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: username,
          password: password,
          name: "test",
        }),
      });
      const json = await response.json();
      const status = response.status;

      if (status === 200) {
        await AsyncStorage.setItem("email", json.email);
        await AsyncStorage.setItem("name", json.name);

        navigation.navigate("Index");
      } else {
        // Handle error
        console.log("Login failed");
      }
    } catch (error) {
      console.error("Error during login:", error);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Username"
        onChangeText={(text) => setUsername(text)}
        value={username}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        onChangeText={(text) => setPassword(text)}
        value={password}
        secureTextEntry
      />
      <View style={styles.Container}>
        <Button title="Submit" onPress={onFinish} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 16,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 16,
    paddingHorizontal: 10,
  },
  Container: {
    alignItems: "center",
    marginBottom: 16,
    marginLeft: 15,
  },
});

export default Login;
