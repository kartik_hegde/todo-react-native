import React, { useState, useContext, useEffect } from "react";
import { Text, Button, View } from "react-native";
import { useNavigation } from "expo-router";
import Context from "../../Context";
import Forms from "../../components/Forms";
import Todos from "../../components/Todos";
import AsyncStorage from '@react-native-async-storage/async-storage';


const Index = () => {
  const navigation = useNavigation();
  const { todos, deleteAll, populate } = useContext(Context);
   const check=async()=>{
        let email= await AsyncStorage.getItem('email')
    if(!email){
      navigation.navigate("Login");
    }
   }
  useEffect(() => {
    check();
    populate();
  }, []);

  // Edit form ...
  const [editFormVisibility, setEditFormVisibility] = useState(false);
  const [editTodo, setEditTodo] = useState("");

  const handleEditClick = (todo) => {
    setEditFormVisibility(true);
    setEditTodo(todo);
  };

  const logout = async() => {
    // Navigate to the login screen
    await AsyncStorage.clear()
    navigation.navigate("Login");
  };

  // Back button
  const cancelUpdate = () => {
    setEditFormVisibility(false);
  };

  return (
    <View>
      <Text
        style={{
          color: "black",
          fontSize: 50,
          textDecorationLine: "underline",
        }}
      >
       
      </Text>
      <Button
        title="logout"
         
        onPress={() => logout()}
      >
      </Button>
      <Forms
        editFormVisibility={editFormVisibility}
        editTodo={editTodo}
        cancelUpdate={cancelUpdate}
      />

      <Todos
        handleEditClick={handleEditClick}
        editFormVisibility={editFormVisibility}
      />

      {todos.length > 1 ? (
        <Button
          className="wrapper"
          type="primary"
          danger
          onPress={() => deleteAll()}
          title="delete all"
        >

        </Button>
      ) : null}

    </View>
  );
};

export default Index;
