import React, { useContext } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import Context from "../Context";
import Checkbox from "expo-checkbox";

const Todos = (props) => {
  const { handleEditClick, editFormVisibility } = props;
  const { todos, handleUpdate, settodos, removeTodo } = useContext(Context);

  const handleUpdateData = async (todo) => {
    updated = [];
    todos.forEach((element) => {
      if (element.id === todo.id) {
        element.completed = !element.completed;
      }
      updated.push(element);
    });
    settodos(updated);
    await handleUpdate(!todo.completed, todo);
  };
  return todos.map((todo) => (
    <View key={todo.id} style={styles.todoBox}>
      <View style={styles.content}>
        {editFormVisibility === false && (
          <Checkbox
            value={todo.completed}
            onValueChange={() => handleUpdateData(todo)}
          />
        )}
        <View style={styles.para}>
          <Text
            style={
              todo.completed
                ? { textDecorationLine: "line-through" }
                : { textDecorationLine: "none" }
            }
          >
            {todo.todo}
          </Text>
        </View>
      </View>
      <View style={styles.actionsBox}>
        {editFormVisibility === false && (
          <>
            <TouchableOpacity onPress={() => handleEditClick(todo)}>
              <MaterialIcons name="edit" size={24} color="black" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => removeTodo(todo)}>
              <MaterialIcons name="delete" size={24} color="black" />
            </TouchableOpacity>
          </>
        )}
      </View>
    </View>
  ));
};

const styles = {
  todoBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  content: {
    flexDirection: "row",
    alignItems: "center",
  },
  para: {
    marginLeft: 10,
  },
  actionsBox: {
    flexDirection: "row",
  },
};

export default Todos;
