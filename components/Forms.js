import React, { useState, useRef, useContext, useEffect } from "react";
import { View, TextInput, Switch, Button, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Context from "../Context";

const Forms = (props) => {
  let { addTodo, handleUpdate } = useContext(Context);
  const inputRef = useRef(null);
  let { editFormVisibility, editTodo, cancelUpdate } = props;

  const [todoValue, setTodoValue] = useState("");
  const [editValue, setEditValue] = useState("");

  useEffect(() => {
    setEditValue(editTodo.todo);
  }, [editTodo]);

  const [input, setInput] = useState(true);

  const handleSubmit = async () => {
    const date = new Date();
    const time = date.getTime();
    const todoObj = {
      id: time,
      todo: todoValue,
      completed: false,
      email: await AsyncStorage.getItem("email"),
    };
    addTodo(todoObj);
    setTodoValue("");
  };

  const editSubmit = () => {
    const editedObj = {
      id: editTodo.id,
      todo: editValue,
      completed: false,
    };
    handleUpdate(false, editedObj);
  };

  return (
    <>
      {editFormVisibility === false ? (
        <View style={styles.container}>
          <Switch value={input} onValueChange={() => setInput(!input)} />

          {input ? (
            <TextInput
              style={styles.input}
              value={todoValue}
              onChangeText={(text) => setTodoValue(text)}
              placeholder="Enter your task"
            />
          ) : (
            <TextInput
              style={styles.textArea}
              value={todoValue}
              onChangeText={(text) => setTodoValue(text)}
              multiline={true}
              placeholder="Enter your task"
            />
          )}

          <Button onPress={handleSubmit} title="ADD" />
        </View>
      ) : (
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            value={editValue}
            onChangeText={(text) => setEditValue(text)}
          />
          <Button onPress={editSubmit} title="UPDATE" />
          <Button onPress={cancelUpdate} title="BACK" />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
  },
  textArea: {
    height: 100,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
  },
});

export default Forms;
